angular.module('color', ['color.filters', 'color.services', 'color.directives', 'color.controllers']).
 	config(['$routeProvider', function($routeProvider) {
		$routeProvider
			.when('/', {
				controller:'SelectColorCtrl',
				templateUrl:'frontpage.html'
			})
			// when redirect to color page
			.when('/:color', {
				controller:'GuessCtrl',
				templateUrl:'guess-color.html'
			})
			.otherwise({
				// redirectTo:'/'
				template: "This doesn't exist"
			});
	}]);