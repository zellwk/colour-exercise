angular.module('color.controllers', []).

controller('SelectColorCtrl', ["$scope", "Toggles", "Modules", function($scope, Toggles, Modules) {

	$scope.showSelect = Toggles.showSelect;
	$scope.redirect = Modules.redirect;
	$scope.autoColor = Modules.generateColor();
	$scope.userColor

	// Need to add this to a keyup event. This is to automatically add a shift before first keypress to let users know its a hex key...
	// if ($scope.userColor) {
	//	$scope.userColor.unshift('#');
	// }

	$scope.openShowSelect = function () {
		$scope.showSelect = true;
	};

	$scope.closeShowSelect = function () {
		$scope.showSelect = false;
	};

	$scope.showMdSelect = false;

	$scope.toggleShowMdSelect = function () {
		if ($scope.showMdSelect == false ) {
			$scope.showMdSelect = true;
		}
		else {
			$scope.showMdSelect = false;
		}
	}
}]).

controller('GuessCtrl', ["$scope", "$routeParams", "$location","PageColor","User", function($scope, $routeParams, $location, PageColor, User) {
	var baseColor = $routeParams.color,
	pattern = /^[0-9a-f]{6}$/,
	bgColor,
	textColor,
	borderColor,
	possibleTextColors = ["hsl(0, 0%, 100%)", "hsl(0, 0%, 0%)"];

	$scope.user = User;
	$scope.pageColor = PageColor;
	$scope.showModWin = false;

	// Resetting user input color to 0
	$scope.user.color.h = 0;
	$scope.user.color.s = 0;
	$scope.user.color.l = 0;

	// Upon initializing, redirect back to '/' if :color not a valid # string (already stripped of #)
	(function () {
		if (!pattern.test(baseColor)) {
			$location.path('/');
		}
	})();

	// convert color into tinycolor format & get other colors
	baseColor = tinycolor(baseColor);
	bgColor = baseColor.toHslString();
	textColor = tinycolor.mostReadable(baseColor,possibleTextColors).toHslString();
	borderColor = textColor;

	// updating PageColor factory with colors
	$scope.pageColor.backgroundColor = bgColor,
	$scope.pageColor.textColor = textColor,
	$scope.pageColor.borderColor = borderColor,
	$scope.pageColor.h = $scope.pageColor.backgroundColor.match(/(\d+)/g)[0]
	$scope.pageColor.s = $scope.pageColor.backgroundColor.match(/(\d+)/g)[1]
	$scope.pageColor.l = $scope.pageColor.backgroundColor.match(/(\d+)/g)[2]

	/**
	 * function to return various CSS Styles depending on what type of input is being called.
	 * 4 choices:
	 * 'background' => background-color
	 * 'text' => 'color'
	 * 'form' => 'background-color', 'color' and 'border-color'
	 * 'submit' => 'background-color' and 'color'
	 */
	 $scope.getColor = function (type) {
	 	switch(type) {
	 		case 'background' : return { "background-color": $scope.pageColor.backgroundColor };

	 		case 'text' : return { "color": $scope.pageColor.textColor };

	 		case 'form' : return {
	 			"background-color": $scope.pageColor.backgroundColor,
	 			"color": $scope.pageColor.textColor,
	 			"border-color": $scope.pageColor.textColor
	 		};

	 		case 'submit' :
	 		if ($scope.pageColor.textColor === possibleTextColors[1]){
	 			return {
	 				"background-color": $scope.pageColor.textColor,
	 				"color": possibleTextColors[0]
	 			};
	 		}
	 		else {
	 			return {
	 				"background-color": $scope.pageColor.textColor,
	 				"color": possibleTextColors[1]
	 			};
	 		}
	 	}
	 };

	 // Gets answer from user
	 $scope.getAnswer = function (h,s,l) {
	 	var h,s,l, HasErrorNan,HasErrorRange;
	 	var error = {
	 		nan: {},
	 		range: {}
	 	};

	 	// Values to test
	 	h = validH(h,'h');
	 	s = validSorL(s,'s');
	 	l = validSorL(l,'l');

	 	HasErrorNan = (Object.getOwnPropertyNames(error.nan).length !== 0);
	 	HasErrorRange = (Object.getOwnPropertyNames(error.range).length !== 0);

	 	if(!HasErrorNan && !HasErrorRange){
	 		$scope.showModWin = true;
	 		// Proceed to record differences in answers
	 	}
	 	else {
	 		alert("Please enter: a number from 0 to 360 for hue and a number from 0 to 100 for saturation and lightness")
	 	}

	 	function validSorL (number, type) {
	 		if(isNaN(number)) {
	 			error.nan[type] = true;
	 		}
	 		else if (number < 0 || number > 100){
	 			error.range[type] = true;
	 		}

	 		return parseInt(number,10);
	 	}

	 	function validH (h,type) {
	 		if(isNaN(h)) {
	 			error.nan[type] = true;
	 		}
	 		else if (h < 0 || h > 360){
	 			error.range[type] = true;
	 		}

	 		return parseInt(h,10);
	 	}
	 }
	 $scope.closeModWin = function () {
	 	$scope.showModWin = false;
	 }
	}]).

controller('ShowAnswerCtrl', ["$scope", "PageColor", "User", "Toggles",
	function($scope, PageColor, User, Toggles) {
		$scope.pageColor = PageColor;
	}]).

controller('ReferenceColorCtrl', ['$scope','Toggles', '$timeout', 'User',function ($scope, Toggles, $timeout, User) {
	$scope.toggles = Toggles;
	$scope.ref = Toggles.ref;
	$scope.user = User;
	$scope.refBgColor = 'hsl(' + $scope.ref.h + ',' + $scope.ref.s + '%,' + $scope.ref.l +'%)';

	$scope.transfer = function () {
		$scope.user.color.h = $scope.ref.h;
		$scope.user.color.s = $scope.ref.s;
		$scope.user.color.l = $scope.ref.l;
	}

	// Watching for Scope ref for changes and updating to refBgColor to update background
	$scope.$watch("ref", function (newValue, oldValue, scope, ref, refBgColor) {
		scope.refBgColor = 'hsl(' + scope.ref.h + ',' + scope.ref.s + '%,' + scope.ref.l +'%)';
	}, true);
}]);