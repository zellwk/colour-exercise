angular.module('color.services', []).
factory('Toggles', [function () {
	var Toggles = {
		showSelect: false,

		ref: {
			h:0,
			s:100,
			l:50
		}
	}
	return Toggles;
}]).

factory('Modules', ['$location', function ($location) {
	var Modules = {
		// redirects to color value
		redirect: function (color) {
			color = color.replace('#','');
			$location.path('/' + color);
		},

		// returns a random 6digit #
		generateColor: function () {
			return (Math.random()*0xFFFFFF<<0).toString(16);
		}
	}

	return Modules;
}]).

service('PageColor', [function (bgColor, textColor, borderColor, h, s, l) {
	this.backgroundColor = bgColor;
	this.textColor = textColor;
	this.borderColor = borderColor;
	this.h = h
	this.s = s
	this.l = l
}]).

factory('User', [function () {
	return {
		color: {
			h: 0,
			s: 0,
			l: 0,
		},
	};
}])