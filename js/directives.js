angular.module('color.directives', []).

directive('stopEvent', function () {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			element.bind(attr.stopEvent, function (e) {
				e.stopPropagation();
			});
		}
	};
}).

directive('ngMousehold', function($timeout) {
	return {
		restrict: 'A',
		link: function(scope, elm, attr, ctrl) {
			var down = false;
			var fn = function () {
				if (!down) return;
				scope.$apply(function() {
					scope.$eval(attr.ngMousehold);
				});

				$timeout(fn, 100);
			};

			elm.bind('mousedown', function () {
				down = true;
				fn();
			});

			elm.bind('mouseup', function () {
				down = false;
			});
		}
	};
}).

directive('ngBlur', function() {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, elm, attr, ctrl) {
			if (attr.type === 'radio' || attr.type === 'checkbox') return;
			elm.bind('blur', function() {
				scope.$apply(function() {
					ctrl.$setViewValue(elm.val());
					scope.$eval(attr.ngBlur);
				});
			});
		}
	};
})